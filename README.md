# android-spectro-doc

Documentation for Android application sm Spectrogram

GOOGLE PLAY (TM) APP URL: https://play.google.com/store/apps/details?id=sm.app.spectro&hl=en_CA

GITLAB PROJECTS URLS

LIBRARY: https://gitlab.com/leafyseadragon/android-acoustic-lib

APPLICATION: https://gitlab.com/leafyseadragon/android-spectro-app

CONTACT: sergemasse1@gmail.com

PRIVACY POLICY: https://gitlab.com/sergemasse/privacy-policy

## Source Code License: ##

  Copyright (c) 2019 Serge Masse
 
  Redistribution and use in source and binary forms, with or without modification, are permitted
  provided that the following conditions are met:
 
  1. Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.
 
  2. Redistributions in binary form must reproduce the above copyright notice, this list of
  conditions and the following disclaimer in the documentation and/or other materials
  provided with the distribution.
 
  3. Neither the name of the copyright holder nor the names of its contributors may be used
  to endorse or promote products derived from this software without specific prior written
  permission.
 
  4. This software, as well as products derived from it, must not be used for the purpose of
  killing, harming, harassing, or capturing animals.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
